from django.shortcuts import render,redirect
from . import models
from . import form 
# Create your views here.
def home(request):
    task_form = form.taskform() 
    if request.method == 'POST':
        task_form = form.taskform(request.POST)
        if task_form.is_valid():
            task_form.save(commit=True)
            return redirect('/')

    tasks = models.Task.objects.all()
    diction = {'task_form':task_form, 'tasks':tasks,'title':'home'}
    return render(request,'home.html',context = diction)

def update(request,pk):
    task = models.Task.objects.get(id=pk)
    task_form = form.taskform(instance=task)
    if request.method == 'POST':
        task_form = form.taskform(request.POST,instance=task)
        if task_form.is_valid():
            task_form.save(commit=True)
            return redirect('/')
    diction = {'task_form':task_form,'title':'Update Task'}
    return render(request,'update.html',context=diction)

def delete(request,pk):
    task = models.Task.objects.get(id=pk)
    if request.method == 'POST':
        task.delete()
        return redirect('/')
    diction = {'task':task,'title':"Delete Task"}
    return render(request,'delete.html',context=diction)
