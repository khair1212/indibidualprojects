from django import forms
from . import models


class taskform(forms.ModelForm):
    class Meta:
        model = models.Task
        fields = '__all__'