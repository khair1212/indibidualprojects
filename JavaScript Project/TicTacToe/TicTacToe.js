var restart = $("#btn");

var squares = $("table tr")

 //clear squares


 function  clearBoard() {
 	for (var i = 0; i <3; i++) {
 		for(var j=0;j<3;j++){
 			squares.eq(i).find('td').eq(j).text('');
 		}
 	}
 	$('h1').text("Welcome to Tic Tac Toe");
 }

restart.on("click",clearBoard);
count = 0 ;

for(var i =0;i<3;i++){
 	for(var j=0;j<3;j++){

	squares.eq(i).find('td').eq(j).on('click',function(){
		count+=1;
		//console.log("Start");
		if(count%2==0){
			$(this).closest("td").text("O");
		}else{ 
			$(this).closest("td").text("X");
		}

		if(Win()==true){
			//console.log("End");
			($('h1').text('Game Finished! Please Restart'));
		}
	})
 	}
 }

 //result check
 function Win(){
 	 if(horizonCheck() == true || vertiCheck() ==true || diagCheck()==true){
 	 	return true;
 	 }else{
 	 	return false;
 	 }
 }

function checkValue(rowIndex,colIndex){
	return squares.eq(rowIndex).find('td').eq(colIndex).text();

}

 //hoizontal check
function horizonCheck(){
	for(var i =0 ; i<3;i++){
 		for(var j=0;j<1 ; j++){
	 		if(checkValue(i,j) ===  checkValue(i,j+1) &&  checkValue(i,j) ===  checkValue(i,j+2) &&  checkValue(i,j) !== undefined && checkValue(i,j) !== ''){
	 			return true;
	 		} 
 		}
 	}
 	return false;
}
//vertical check
function vertiCheck(){
	for(var i =0 ; i<1;i++){	
 		for(var j=0;j<3 ; j++){	
	 		if(checkValue(i,j) ===  checkValue(i+1,j) &&  checkValue(i,j) ===  checkValue(i+2,j) &&  checkValue(i,j) !== undefined &&  checkValue(i,j) !==''){
	 			return true;	
	 		}
 		}	
 	}
 	return false;
}

 //diagonal check
function diagCheck(){
	for(var i = 0 ; i<1; i++){
 	for(var j=0; j<1 ; j++){
 		if( checkValue(i,j) === checkValue(i+1,j+1) &&  checkValue(i,j) === checkValue(i+2,j+2) &&  checkValue(i,j)!== undefined &&  checkValue(i,j) !=='')
 		{
 			return true ;
 		}else if( checkValue(i+2,j) ===  checkValue(i+1,j+1) && checkValue(i+2,j) === checkValue(i,j+2) && checkValue(i+2,j) !== undefined && checkValue(i+2,j) !=='')
 		{
 			return true;
 		}else{
 			return false;
 		}
 	}
 }
}