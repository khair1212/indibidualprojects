console.log("HI");
var player1 = prompt("1st(blue) player name:");
var p1color = 'rgb(86, 151, 255)';
var player2 = prompt("2nd(red) player name:");
var p2color = 'rgb(237, 45, 73)';

var game_on = true;
var table = $('table tr');

function reprotWin(row,col){
	console.log(row);
	console.log(col);
}

function changecolor(rowIndex,colIndex,color) {
	return table.eq(rowIndex).find('td').eq(colIndex).find('button').css('background-color',color);
}
function reportcolor(rowIndex,colIndex) {
	//console.log(table.eq(rowIndex).find('td').eq(colIndex).find('button').css('background-color'))
	return table.eq(rowIndex).find('td').eq(colIndex).find('button').css('background-color');
}

function checkBottom(colIndex){
	var bottom = reportcolor(5,colIndex);
	for(var i = 5; i>=0 ; i--){
		bottom = reportcolor(i,colIndex);
		if (bottom === 'rgb(128, 128, 128)') {
			return i;
		}
	}
}

function colorMatch(one,two,three,four){
	return (one===two && one === three && one===four && one !== 'rgb(128, 128, 128)' && one!== undefined);
}

function horizontalWinCheck(){
	for(var row = 0 ; row<6 ; row++){
		for(var col = 0 ; col<4 ; col++){
			if (colorMatch(reportcolor(row,col),reportcolor(row,col+1),reportcolor(row,col+2),reportcolor(row,col+3))) {
				return true ; 
			}else{
				continue;
			}
		}
	}
}

function verticalCheck() {
  for (var col = 0; col < 7; col++) {
    for (var row = 0; row < 3; row++) {
      if (colorMatch(reportcolor(row,col), reportcolor(row+1,col) ,reportcolor(row+2,col), reportcolor(row+3,col))) {
        return true;
      }else {
        continue;
      }
    }
  }
}

function diagCheck(){
	for(var col =  0 ; col < 5 ; col ++){
		for(var row = 0 ; row<7 ; row++){
			if(colorMatch(reportcolor(row,col),reportcolor(row+1,col+1),reportcolor(row+2,col+2),reportcolor(row+3,col+3))){
				reprotWin(row,col);
				return true;
			}else if(colorMatch(reportcolor(row,col),reportcolor(row-1,col+1),reportcolor(row-2,col+2),reportcolor(row-3,col+3))){
				reprotWin(row,col);
				return true;	
			}else{
				continue;
			}
			
		}
	}
}

//start with player 1

var currentplayer = 1 ;
var currentname = player1;
var currentcolor = p1color;

$("h3").text(currentname+"It's your turn ! please pick a column to drop your blue chip!");


$(".board button").on('click',function(){
    //get the closen column
	var col = $(this).closest("td").index();
	//get available circle
	var bottom = checkBottom(col);
	//change color
	changecolor(bottom,col,currentcolor);

   
    // Check for a win or a tie.
  if (horizontalWinCheck() || verticalCheck() || diagCheck()) {
    gameEnd(currentname);
  }

  // If no win or tie, continue to next player
  currentplayer = currentplayer * -1 ;

  // Re-Check who the current Player is.
  if (currentplayer === 1) {
    currentname = player1;
    $('h3').text(currentname+": it is your turn, please pick a column to drop your blue chip.");
    currentcolor = p1color;
  }else {
    currentname = player2
    $('h3').text(currentname+": it is your turn, please pick a column to drop your red chip.");
    currentcolor = p2color;
  }
})
function gameEnd(winningPlayer) {
  for (var col = 0; col < 7; col++) {
    for (var row = 0; row < 7; row++) {
      $('h3').fadeOut('fast');
      $('h2').fadeOut('fast');
      $('h1').text(winningPlayer+" has won! Refresh your browser to play again!").css("fontSize", "50px")
    }
  }
}